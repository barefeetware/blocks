//
//  Product.swift
//  Blocks
//
//  Created by Tom Brodhurst-Hill on 6/7/21.
//  Copyright © 2021 BareFeetWare. All rights reserved.
//

import Foundation

struct Product: Identifiable {
    let id: Int
    let name: String
    let description: String
    let price: Double
    let imageName: String
}
