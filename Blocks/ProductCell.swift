//
//  ProductCell.swift
//  Blocks
//
//  Created by Tom Brodhurst-Hill on 26/6/21.
//  Copyright © 2021 BareFeetWare. All rights reserved.
//

import SwiftUI

struct ProductCell: View {
    
    let image: Image
    let text: Text
    let detailText: Text
    let tertiaryText: Text
    
    var body: some View {
        HStack {
            image
                .resizable(resizingMode: .stretch)
                .aspectRatio(contentMode: .fit)
                .frame(width: 60.0)
            VStack(alignment: .leading) {
                text
                    .font(.title2)
                    .fontWeight(.bold)
                detailText
                    .foregroundColor(Color.gray)
                HStack {
                    tertiaryText
                    Button(action: /*@START_MENU_TOKEN@*//*@PLACEHOLDER=Action@*/{}/*@END_MENU_TOKEN@*/) {
                        Text(/*@START_MENU_TOKEN@*/"Buy"/*@END_MENU_TOKEN@*/)
                    }
                }
            }
        }
    }
}

struct ProductCell_Previews: PreviewProvider {
    static var previews: some View {
        ProductCell(
            image: Image(systemName: "photo"),
            text: Text(/*@START_MENU_TOKEN@*/"Text"/*@END_MENU_TOKEN@*/),
            detailText: Text(/*@START_MENU_TOKEN@*/"Detail Text"/*@END_MENU_TOKEN@*/),
            tertiaryText: Text("Tertiary Text")
        )
        .previewLayout(.sizeThatFits)
    }
}
