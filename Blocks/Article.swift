//
//  Article.swift
//  Blocks
//
//  Created by Tom Brodhurst-Hill on 5/3/21.
//  Copyright © 2021 BareFeetWare. All rights reserved.
//

import Foundation

struct Article: Identifiable {
    let id: Int
    let title: String
    let date: Date
    let detail: String
    let smallImageName: String
    let largeImageName: String
}
