//
//  ProductCell+Product.swift
//  Blocks
//
//  Created by Tom Brodhurst-Hill on 6/7/21.
//  Copyright © 2021 BareFeetWare. All rights reserved.
//

import SwiftUI

extension ProductCell {
    init(product: Product) {
        self.init(
            image: Image(product.imageName),
            text: Text(product.name),
            detailText: Text(product.description),
            tertiaryText: Text(String(describing: product.price))
        )
    }
}

struct ProductCell_Product_Previews: PreviewProvider {
    static var previews: some View {
        ProductCell(product: .spaceShuttle)
            .previewLayout(.sizeThatFits)
    }
}
