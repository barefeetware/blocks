//
//  BlocksApp.swift
//  Blocks
//
//  Created by Tom Brodhurst-Hill on 4/3/21.
//

import SwiftUI

@main
struct BlocksApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
